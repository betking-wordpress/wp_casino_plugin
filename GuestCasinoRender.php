<?php

namespace  PluginCasino;

include_once CASINO_PLUGIN_ROOT.'/util/Proxies/BannerServiceProxy.php';
include_once CASINO_PLUGIN_ROOT.'/util/Proxies/CasinoLobbyServiceProxy.php';
include_once CASINO_PLUGIN_ROOT.'/util/Mobile_Detect.php';
include_once CASINO_PLUGIN_ROOT.'/util/FileCache.php';
include_once 'CasinoSettingsManager.php';

use FileCache;
use PluginCasino\util\Mobile_Detect;
use PluginCasino\util\Proxies\BannerServiceProxy;
use PluginCasino\util\Proxies\CasinoLobbyServiceProxy;

class GuestCasinoRender{

    use FileCache;

    private $book_id;
    private $casino_lobby_servie;
    private $casino_settings;

    public function __construct()
    {
        $this->casino_settings        = new CasinoSettingsManager();
        $this->casino_lobby_service   = new CasinoLobbyServiceProxy();
        $this->book_id                = $this->casino_settings->casinoSetting('book_id');
    }

    public function renderCasino($current_tab, $platform){
        $context = [];
        $context['casino_banner_slider'] = $this->getCasinoMainSlider();
        $context['casino_categories']    = $this->getTabsCasino();
        $context['jackpot']              = $this->getCasinoJackpotLobby();
        $context['casino_games']         = $this->getCasinoGames($current_tab, $platform);
        $context['casino_url']           = $this->casino_settings->casinoSetting('casino_data_url');
        $context['book_id']              = $this->book_id;
        $context['current_tab']          = $current_tab;

        return $context;
    }

    private function getCasinoMainSlider(){
        $banner_service = new BannerServiceProxy();
        return $banner_service->getBannersSlider();
    }

    private function getTabsCasino(){
        $result = [];
        $casino_tabs_data = json_decode(file_get_contents(CASINO_PLUGIN_ROOT.'/data/casino_tabs.json'),true);
        $categories = $casino_tabs_data['data']['categories'];
        foreach($categories as $category){
            $category['url_category'] = str_replace(' ','-',strtolower (gettext($category['name'])));
            array_push($result, $category);
        }
        return $result;
    }

    private function getCasinoGames($current_tab, $platform){
        return  $this->getGamesFileByCategoryAndPlatform($current_tab, $platform);
    }

    private function getCasinoJackpotLobby(){
        $result = [];
        $jackpots_data              = json_decode(file_get_contents(CASINO_PLUGIN_ROOT.'/data/casino_jackpot.json'),true);
        $result['quick_jackpot']    = $this->getAmountJackpot($jackpots_data['data']['quick_jackpot']['amount'], 5);
        $result['daily_jackpot']    = $this->getAmountJackpot($jackpots_data['data']['daily_jackpot']['amount'], 5);
        $result['megajackpot']      = $this->getAmountJackpot($jackpots_data['data']['megajackpot']['amount']);
        $result['games_1']          = [];
        $result['games_2']          = [];
        for($i = 0; $i < 4; $i++){
            array_push($result['games_1'],  $jackpots_data['data']['games'][$i]);
        }
        for($i = 4; $i < 8; $i++){
            array_push($result['games_2'], $jackpots_data['data']['games'][$i]);
        }

        return $result;
    }

    private function getAmountJackpot($amount, $total_spots = 7){
        $values = strval( $amount );
        $array_values = str_split($values);
        while(count($array_values) < $total_spots){
            array_unshift($array_values, '0');
        }
        return $array_values;
    }

    private function getGamesFileByCategoryAndPlatform($category, $platform){
        if(file_exists(__DIR__ . '/data/games/casino_games_'.$category.'_'.$platform.'.json')){
            $result = json_decode(file_get_contents(__DIR__ . '/data/games/casino_games_'.$category.'_'.$platform.'.json'));
            return $result->data;
        }else{
            return [];
        }
    }

}

