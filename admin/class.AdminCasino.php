<?php
namespace PluginCasino\admin;

include_once CASINO_PLUGIN_ROOT.'/CasinoSettingsManager.php';

use PluginCasino\CasinoSettingsManager;

class AdminCasino{

    private $setting;

    public function __construct()
    {
        $this->setting = new CasinoSettingsManager();
    }

    private function reloadSettings(){
        $this->setting = new CasinoSettingsManager();
    }

    public function settingsCasino(){
        if( $_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)){
            $this->setting->saveSettings($_POST);
            $this->reloadSettings();

        }
        return $this->showSetting();
    }

    private function showSetting(){
        $context           = $this->setting->getAllSettings();
        $context['themes'] = $this->setting->getThemes();
        $context['brands'] = $this->setting->getBrands();
        $context['books']  = $this->setting->getBooks();

        return $context;
    }

}