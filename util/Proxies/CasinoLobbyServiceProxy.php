<?php
namespace PluginCasino\util\Proxies;

include_once __DIR__.'/../../CasinoSettingsManager.php';

use PluginCasino\CasinoSettingsManager;

class CasinoLobbyServiceProxy{

    private $base_url;
    private $settings;

    public function __construct(){
        $this->settings = new CasinoSettingsManager();
        $this->base_url = $this->settings->casinoSetting('casino_data_url', 'http://casinolobbyservice.coolwager.com/api/v1/');
    }

    public function getCasinoTabs(){
        $path = 'categories/by_brand/book/'.$this->settings->casinoSetting('book_id');
        return $this->makeRequest($path);
    }

    public function getCasinoGamesByCategoryAndPlatform($category, $platform){
        $path       = 'games';
        $options    = ['categoryId'=>$category,'isActive'=>1,'platformId' => $platform];
        return $this->makeRequest($path, 'GET', $options);
    }

    public function getCasinoJackpotLobby(){
        $path       = 'jackpot/lobby';
        return $this->makeRequest($path);
    }

    public function getUrlGame($book_id, $game_id){
        $path = 'launch/guest/'.$book_id.'/'.$game_id;
        $response = $this->makeRequest($path);
        return json_decode($response);
    }


    private function makeRequest($path, $method='GET', $options = [])
    {
        try {
            $url    = $this->base_url.$path;
            if($method=='POST'){
                $parameters     = http_build_query( $options );
                $requestOptions = [
                    'http' => [
                        'method'  => $method,
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $parameters
                    ]
                ];
                $requestContext = stream_context_create( $requestOptions );
                $response       = file_get_contents( $url, null, $requestContext );
            }
            else{
                $response = file_get_contents($url.'?'.http_build_query($options));
            }

            if ( $response === false ) {
                error_log( 'Request to Casino Lobby failed. Request options: ' . json_encode( $options ) . 'URL: ' . $url );
                throw new \Exception();
            }

            return $response;
        } catch (\Exception $e) {
            error_log( 'Error in makeRequestCasinoLobby. Request options: ' . json_encode( $options ) . 'URL: ' . $url );
            return 'Fail, error request: '.$e->getMessage();
        }

    }

}

