<?php
namespace PluginCasino\util\Proxies;

include_once CASINO_PLUGIN_ROOT.'/CasinoSettingsManager.php';

use PluginCasino\CasinoSettingsManager;

class BannerServiceProxy{

    private $settings;
    private $base_url;
    private $token;

    public function __construct(){
        $this->settings       = new CasinoSettingsManager();
        $this->base_url = $this->settings->casinoSetting('banner_service_url','http://bannerservice.coolwager.com/api/v1/zones/');
        $this->token    = $this->settings->casinoSetting('banner_service_token', 'YamnYaR6Akwahlp4cYmEB2eLyNnXNmIt0fpEJf7qTIw=1');
    }

    public function getBannersSlider(){
        try {
            date_default_timezone_set('America/New_York');
            $zone_id  = $this->settings->casinoSetting('banners_id_zone', '55');
            $path = 'zones/'. $zone_id;
            $response =  $this->makeRequest($path);
            $now = date('Y-m-d H:i:s');
            $formatArray = array_values(array_filter($response['zone']['banners'], function ($banner) use ($now)
            { return strtotime($banner['date_start']) <= strtotime($now) && strtotime($banner['date_end']) >= strtotime($now); }
            ));

            return $formatArray;
        } catch (\Exception $e) {
            error_log("BannerTool Error!:" . $e , 0);
            error_log("Class BannerToolProxy"   , 0);
            error_log("Line Of Code:9"          , 0);
            return [];
        }

    }


    private function makeRequest($path, $method='GET', $options = [])
    {
        try {
            $url    = $this->base_url.$path.'?token='.$this->token;

            if($method=='POST'){
                $parameters     = http_build_query( $options );
                $requestOptions = [
                    'http' => [
                        'method'  => $method,
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $parameters
                    ]
                ];
                $requestContext = stream_context_create( $requestOptions );
                $response       = file_get_contents( $url, null, $requestContext );
            }
            else{
                $response = file_get_contents($url.'&'.http_build_query($options));
            }

            if ( $response === false ) {
                error_log( 'Request to Banner Service failed. Request options: ' . json_encode( $options ) . 'URL: ' . $url );
                throw new \Exception();
            }

            return json_decode($response,true);
        } catch (\Exception $e) {
            error_log( 'Error in makeRequestBannerService. Request options: ' . json_encode( $options ) . 'URL: ' . $url );
            return 'Fail, error request: '.$e->getMessage();
        }

    }
}
