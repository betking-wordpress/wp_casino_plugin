<?php
/*
Plugin Name: CasinoLobby Plugin
Plugin URI: https://jazzsports.ag/
Description: Casino Lobby plugin
Version: 0.1
Author: dbolanos
Author URI: https://jazzsports.ag
License: GPLv2 or later
Text Domain: Casino
*/

define( 'CASINO_PLUGIN_ROOT', plugin_dir_path( __FILE__ ) );

include_once CASINO_PLUGIN_ROOT . '/admin/class.AdminCasino.php';
include_once CASINO_PLUGIN_ROOT . '/GuestCasinoRender.php';

use PluginCasino\admin\AdminCasino;
use PluginCasino\GuestCasinoRender;
use PluginCasino\util\Mobile_Detect;

function load_scripts($version = 1.0)
{
    wp_register_script('sliders-js', "/wp-content/plugins/casino_plugin/resources/js/sliders.js",
        [], $version, true);
    wp_enqueue_script('sliders-js');

    wp_register_script(
        'modal-game-casino', '/wp-content/plugins/casino_plugin/resources/js/modeModalGame.js',
        array(), '4.0.2', true
    );
    wp_enqueue_script('modal-game-casino');
}

function renderGuestCasino($atts)
{
    if(isset($atts['casino-tab']) && !empty($atts['casino-tab'])){
        $current_tab = $atts['casino-tab'];
    }
    else{
        $current_tab = 'lobby';
    }
    $guest_casino   = new GuestCasinoRender();
    $platform = getBetsoftMobileVersion();
    $cachePathCasino = __DIR__.'/cache/categories/casino_page_'.$current_tab.'_'.$platform .'.html';
    $validateCache= isset($_GET['cache']) ? false : true;
    //CACHE TIME MODIFIES EVERY 20 MIN
    if(FileCache::isFileCacheValid(1200,$cachePathCasino) && $validateCache ){
        print FileCache::getCacheFile($cachePathCasino);
    }
    else{
        $context        = $guest_casino->renderCasino($current_tab,$platform);
        if($current_tab == 'lobby'){
            $result = Timber::render( 'layout/casino/default/lobby.twig', $context );
        }
        else{
            $result = Timber::render( 'layout/casino/default/casino_page.twig', $context );
        }
        //THIS FUNCTION CREATE THE CACHE
        FileCache::cacheFile($cachePathCasino,  $result, true);
    }

    load_scripts();
}

add_shortcode('render_guest_casino', 'renderGuestCasino');


function getBetsoftMobileVersion()
{

    $detect = new Mobile_Detect ();
    if ($detect->isAndroidOS()) {
        $platform = 2;
    } elseif ($detect->isWindowsPhoneOS()) {
        $platform = 3;
    } elseif ($detect->isiOS()) {
        $platform = 4;
    } else {
        $platform = 1;
    }

    return $platform;
}

//============================================================================================
// PLUGIN CASINO SETTING
//============================================================================================

if(is_admin()) {
    add_action("admin_menu", "add_casino_setting");
}

function add_casino_setting()
{
    add_options_page("Casino Setting", "Casino Setting", "manage_options", "casino-setting", "casino_setting", null, 99);
}

function casino_setting()
{
    $casino_setting = new AdminCasino();
    Timber::render('admin/resources/html/settingMain.twig', $casino_setting->SettingsCasino());

}


