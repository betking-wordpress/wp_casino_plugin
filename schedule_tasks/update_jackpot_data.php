<?php

use PluginCasino\data_updaters\CasinoJackpotDataUpdater;

require_once(__DIR__ . '/../data_updaters/class.CasinoJackpotDataUpdater.php');


$targetFileCasinoTabs          = __DIR__ . '/../data/casino_jackpot.json';

$casino_tabs_update = new CasinoJackpotDataUpdater($targetFileCasinoTabs);
$casino_tabs_update->startGetJackpots();