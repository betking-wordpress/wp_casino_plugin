<?php

use PluginCasino\data_updaters\CasinoGamesUpdater;

require_once(__DIR__ . '/../data_updaters/class.CasinoGamesUpdater.php');


$targetFileCasinoGamesDesktop          = __DIR__ . '/../data/games/casino_games_';

$casino_games = new CasinoGamesUpdater($targetFileCasinoGamesDesktop, 4);
$casino_games->startGetGames();