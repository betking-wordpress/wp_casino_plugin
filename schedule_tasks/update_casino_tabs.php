<?php

use PluginCasino\data_updaters\CasinoTabsDataUpdater;

require_once(__DIR__ . '/../data_updaters/class.CasinoTabsDataUpdater.php');


$targetFileCasinoTabs          = __DIR__ . '/../data/casino_tabs.json';

$casino_tabs_update = new CasinoTabsDataUpdater($targetFileCasinoTabs);
$casino_tabs_update->startGetTabs();