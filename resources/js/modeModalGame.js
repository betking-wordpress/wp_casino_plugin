jQuery(document).ready(function () {

    function requestUrlGame(game_url) {
        return new Promise((resolve, reject) => {
            jQuery.ajax({
                url: game_url,
            })
                .done(function(data) {
                    console.log('success');
                    resolve(data);
                })
                .fail(function(status){
                    console.log('fail game request, ' + status);
                    reject(null);
                });
        })
    }

    jQuery('.show_game').on('click', function(e){
        e.preventDefault();
        jQuery("body").addClass("modal-game-open");
        let game_url    = jQuery(this).data('game_url');
        let game_name   = jQuery(this).data('game_title');

        requestUrlGame(game_url)
            .then((data) => {
                if (data.data.url != null){
                    var iframe_game = "<iframe title='"+ game_name +"' class='embed-responsive-item' src='"+ data.data.url +"'></iframe>'";

                    jQuery('#game_name').html(game_name);
                    jQuery('#content_game').html(iframe_game);
                    jQuery('#modal_game').modal('show');
                }
            })
            .catch((error) => {
                console.log('Request game casino fail');
            });
    });

    jQuery('#modal_game').on('hidden.bs.modal', function () {
        jQuery('#game_name').empty();
        jQuery('#content_game').empty();
        jQuery("body").removeClass("modal-game-open")
    });

});