jQuery(document).ready(function () {

    console.log('Loading sliders..');

    jQuery('.carousel').each(function(){
        console.log(this.id);
        createSlider(this.id);
    });

    if ( jQuery( "#casinoMainSliderLobby" ).length ) {
        jQuery('#casinoMainSliderLobby').carousel(1000);
    }

    if ( jQuery( "#featuredGamesSlider" ).length ) {
        jQuery('#featuredGamesSlider').carousel(2000);
    }

function createSlider(name){
    jQuery('#'+name+' .carousel-item').each(function () {
        let minPerSlide = 4;
        let next = jQuery(this).next();
        if (!next.length) {
            next = jQuery(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo(jQuery(this));

        for (let i = 0; i < minPerSlide; i++) {
            next = next.next();
            if (!next.length) {
                next = jQuery(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo(jQuery(this));
        }
    });
}


});
