<?php

namespace PluginCasino\data_updaters;

use PluginCasino\data_updaters\CasinoUpdater;
use PluginCasino\util\Proxies\CasinoLobbyServiceProxy;

require_once __DIR__ . '/../util/Proxies/CasinoLobbyServiceProxy.php';
require_once __DIR__ . '/class.CasinoUpdater.php';

class CasinoTabsDataUpdater {

   private $casino_proxy;
   private $target_file;

   public function __construct($targetFile)
   {
       $this->target_file = $targetFile;
       $this->casino_proxy = new CasinoLobbyServiceProxy();
   }

    public function  startGetTabs(){
        $response      = $this->getUpdatedData();
        if($this->hasContentChanged($this->target_file, $response)){
            $success = file_put_contents($this->target_file,$response);
            if($success === false){
                error_log('Failed to write data to target file'.$this->target_file);
                echo 'Failed to write data to target file'. json_encode($this->target_file);
            }else{
                echo "Task executed successfully";
            }
        }else{
            echo 'No changes in the data';
        }
    }

    /**
     * @inheritDoc
     */
    function getUpdatedData($options = null)
    {
        return $this->casino_proxy->getCasinoTabs();
    }

    public function hasContentChanged($target_file, $newContent)
    {
        if(file_exists($target_file)){
            return md5(file_get_contents($target_file)) !== md5($newContent);
        }
        return true;
    }


}
