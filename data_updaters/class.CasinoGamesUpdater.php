<?php

namespace PluginCasino\data_updaters;

use PluginCasino\util\Proxies\CasinoLobbyServiceProxy;

require_once __DIR__ . '/../util/Proxies/CasinoLobbyServiceProxy.php';
require_once __DIR__ . '/class.CasinoUpdater.php';

class CasinoGamesUpdater{

    private $platform;
    private $target_file;
    private $casino_proxy;

    public function __construct($targetFile, $platform)
    {
        $this->target_file  = $targetFile;
        $this->platform     = $platform;
        $this->casino_proxy = new CasinoLobbyServiceProxy();
    }

    public function startGetGames(){
        $categories = $this->getGamesAllCategoryActivesByPlatform();
        foreach($categories as $category){
            $response =  $this->getUpdatedData($category['id'], $this->platform);
            $path = $this->target_file.str_replace(' ','-',strtolower (gettext($category['name']))).'_'.$this->platform.'.json';
            if($this->hasContentChanged($path, $response)){
                $success = file_put_contents($path,$response);
                if($success === false){
                    error_log('Failed to write data to target file'.$path);
                    echo 'Failed to write data to target file'. json_encode($path);
                }else{
                    echo "Task executed successfully";
                }
            }else{
                echo 'No changes in the data';
            }
        }
    }

    private function getGamesAllCategoryActivesByPlatform(){
        $response = [];
        $categories = $this->getCasinoTabsData();
        foreach($categories as $category){
            if($category['is_active']){
                array_push($response,$category);
            }
        }
        return $response;
    }

    private function getCasinoTabsData(){
        $path = __DIR__ . '/../data/casino_tabs.json';
        $file = file_get_contents($path);
        if($file !== false){
            $response = json_decode($file, true);
            return $response['data']['categories'];
        }
    }

    /**
     * @inheritDoc
     */
    private function getUpdatedData($category, $platform)
    {
        return $this->casino_proxy->getCasinoGamesByCategoryAndPlatform($category, $platform);
    }

    private function hasContentChanged($target_file, $newContent)
    {
        if(file_exists($target_file)){
            return md5(file_get_contents($target_file)) !== md5($newContent);
        }
        return true;
    }
}