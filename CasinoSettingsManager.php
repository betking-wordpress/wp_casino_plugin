<?php

namespace PluginCasino;

class CasinoSettingsManager
{

    private $casinoSetting;

    /**
     * Setting.
     */
    public function __construct()
    {
        $this->casinoSetting    = $this->readJson('casino_setting');
    }


    /**
     * @return array
     * @comment THIS FUNCTION RETURNS THE SETTING OF THE THEME.
     * @comment IMPORTANT IF THERE IS NOT SUCH SETTING THE FUNCTION RETURNS AN EMPTY ARRAY
     */
    private function readJson($name)
    {
        $pathThemeSetting = __DIR__ . '/conf/'.$name.'.json';
        if (file_exists($pathThemeSetting)) {
            $themeSetting = file_get_contents($pathThemeSetting);
            return json_decode($themeSetting,true);
        }

        return array();
    }

    /**
     * @param $value
     * @param string $defaultValue
     * @return string
     */
    public function casinoSetting($value, $defaultValue='')
    {
        return $this->casinoSetting[$value] ?? $defaultValue;
    }

    public function getAllSettings(){
        return $this->casinoSetting;
    }

    public function saveSettings($post){
            $setting = str_replace( '\r\n', 'new_line', json_encode($post));
            $setting = stripslashes($setting);
            $setting = str_replace('new_line', '\r\n', $setting);

            file_put_contents(__DIR__ . '/conf/'.$post['nameJson'].'.json',$setting);
    }

    public function getBrands(){
        return ['jackpotkings.ag'];
    }

    public function getThemes(){
        return ['casino'];
    }

    public function getBooks(){
        return [['id' => '796', 'name' => 'jackpotkings.ag']];
    }
}